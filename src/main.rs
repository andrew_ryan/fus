#[allow(warnings)]
pub fn find(name: &str) -> Vec<String> {
    use walkdir::WalkDir;
    let mut files = vec![];
    for entry in WalkDir::new("./").into_iter().filter_map(|e| e.ok()) {
        let path = format!("{}", entry.path().display());
        if path.ends_with(name) {
            files.push(path);
        }
    }
    files.sort_by_key(|s|{
        let ss = s.to_string();
        ss.chars()
        .map(|d|(d as u8) as u64)
        .into_iter()
        .sum::<u64>()
    });
    files
}

use clap::{App, Arg, SubCommand};

fn main() {
    let matches = App::new("fus")
                          .version("1.0")
                          .about("Fast find file or Remove")
                          .arg(Arg::with_name("name")
                               .short("n")
                               .long("name")
                               .value_name("NAME")
                               .help("filter files by name\nExample:\nfus -n .toml => find all *.toml in current dir")
                               .required(true)
                               .takes_value(true))
                          .subcommand(SubCommand::with_name("rm")
                                .about("Remove the files (is not required)\nExample:\nfus -n .toml rm => find all *.toml in current dir and remove")
                                .help("Remove the files"))
                                .get_matches();
    use doe::*;
    let name = matches.value_of("name").unwrap();
    let rm = matches.subcommand().0;
    let mut re = vec![];
    let result = find(name);
    re.extend_from_slice(&result);
    if rm == "rm" {
        for i in re.iter() {
            println!("remove :{:?}", i);
            remove_file_or_folder!(i);
        }
    } else {
        println!("{:?}", re);
    }
}
