# Fast find file or Remove cli
[![Crates.io](https://img.shields.io/crates/v/fus.svg)](https://crates.io/crates/fus)
[![Rust](https://img.shields.io/badge/rust-1.56.1%2B-blue.svg?maxAge=3600)](https://gitlab.com/andrew_ryan/fus)
[![license](https://img.shields.io/badge/license-MIT-blue.svg)](https://gitlab.com/andrew_ryan/fus/-/raw/master/LICENSE)
```
fus 1.0
Fast find file or Remove

USAGE:
    fus --name <NAME> [SUBCOMMAND]

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -n, --name <NAME>    filter files by name
                         Example:
                         fus -n .toml => find all *.toml in current dir

SUBCOMMANDS:
    help    Prints this message or the help of the given subcommand(s)
    rm      Remove the files (is not required)
            Example:
            fus -n .toml rm => find all *.toml in current dir and remove
```
## install
```
cargo install fus
```

## download bin
``` 
#linux
wget https://gitlab.com/andrew_ryan/fus/-/raw/master/bin/fus
#windows
wget https://gitlab.com/andrew_ryan/fus/-/raw/master/bin/fus.exe
```
